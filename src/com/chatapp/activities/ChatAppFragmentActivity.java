package com.chatapp.activities;

import java.util.ArrayList;
import java.util.List;

import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.chatapp.R;
import com.chatapp.adapters.DrawerAdapter;
import com.chatapp.fragments.FriendsFragment;
import com.chatapp.fragments.MessagesFragment;
import com.chatapp.fragments.SigninFragment;
import com.chatapp.fragments.SignupFragment;
import com.chatapp.fragments.SplashFragment;
import com.chatapp.model.DrawerItem;

public class ChatAppFragmentActivity extends FragmentActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	private ActionBarDrawerToggle mActionBarDrawerToggle;
	private String mDrawerTitle;
	private String mTitle;
	private List<DrawerItem> items;
	private FragmentManager fManager;
	private Fragment fragment;
	private DrawerAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedBundle) {
		super.onCreate(savedBundle);
		setContentView(R.layout.activity_chat_app_fragment);
		mTitle = mDrawerTitle = getTitle().toString();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerListView = (ListView) findViewById(R.id.left_drawer);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		fManager = getSupportFragmentManager();
		initListItems();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayUseLogoEnabled(false);
		getActionBar().setIcon(android.R.color.transparent);
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#61ADAB")));
		// getActionBar().setHomeButtonEnabled(true);
		mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.nav_drawer_chat_app, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);

		if (savedBundle == null) {
			selectItem(0);
		}
	}

	private void initListItems() {
		items = new ArrayList<DrawerItem>();
		Drawable[] drawables = new Drawable[] {
				getResources().getDrawable(R.drawable.profile),
				getResources().getDrawable(R.drawable.friends),
				getResources().getDrawable(R.drawable.chat),
				getResources().getDrawable(R.drawable.checkin),
				getResources().getDrawable(R.drawable.logout),
				getResources().getDrawable(R.drawable.setting)

		};
		CharSequence[] charSequence = new CharSequence[] {
				getResources().getString(R.string.profile),
				getResources().getString(R.string.friends),
				getResources().getString(R.string.messages),
				getResources().getString(R.string.checkin),
				getResources().getString(R.string.logout),
				getResources().getString(R.string.setting) };
		mAdapter = new DrawerAdapter(this, R.layout.drawer_item_layout, items);
		items.clear();
		DrawerItem item = null;
		for (int i = 0; i < drawables.length; i++) {
			item = new DrawerItem();
			item.setDrawable(drawables[i]);
			item.setTitle(charSequence[i]);
			items.add(item);
		}
		item = null;
		mDrawerListView.setAdapter(mAdapter);
		mDrawerListView.setOnItemClickListener(new DrawerItemClickListener());
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return true;
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		int key = position + 1;
		switch (key) {
		case 1:
			fragment = new SplashFragment();
			break;
		case 2:
			fragment = new FriendsFragment();
			break;
		case 3:
			fragment = new MessagesFragment();
			break;
		case 4:
			fragment = new SignupFragment();
			break;
		}
		fManager.beginTransaction().replace(R.id.content_frame, fragment)
				.commit();

		mDrawerListView.setItemChecked(position, true);
		setTitle(items.get(position).getTitle());
		mDrawerLayout.closeDrawer(mDrawerListView);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title.toString();
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mActionBarDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mActionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

}
