package com.chatapp.model;

import android.graphics.drawable.Drawable;

public class Message {
	private Drawable avatar;
	private String from;
	private String message;

	public Drawable getAvatar() {
		return avatar;
	}

	public void setAvatar(Drawable avatar) {
		this.avatar = avatar;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
