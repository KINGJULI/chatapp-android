package com.chatapp.adapters;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chatapp.R;
import com.chatapp.model.Friend;

public class FriendsAdapter extends ArrayAdapter<Friend> {
	private List<Friend> mFriends;
	private LayoutInflater mInflater;
	private int mResource;
	private Context mContext;

	public FriendsAdapter(Context context, int resource, List<Friend> friends) {
		super(context, resource, friends);
		this.mContext = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mFriends = friends;
		this.mResource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FriendHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(mResource, parent, false);
			holder = new FriendHolder();
			holder.fAvatar = (ImageView) convertView
					.findViewById(R.id.friend_avatar);
			holder.fName = (TextView) convertView
					.findViewById(R.id.friend_name);
			holder.fLogin = (TextView) convertView
					.findViewById(R.id.friend_login);
			convertView.setTag(holder);
		} else {
			holder = (FriendHolder) convertView.getTag();
		}

		Friend friend = mFriends.get(position);
		holder.fAvatar.setImageDrawable(mContext.getResources().getDrawable(
				R.drawable.profile));
		holder.fName.setText(friend.getName());
		holder.fLogin.setText(friend.getLogin());
		Log.d("CurrentUser", "" + friend.getName());
		return convertView;

	}

	private static class FriendHolder {
		private ImageView fAvatar;
		private TextView fName;
		private TextView fLogin;
	}
}
