package com.chatapp.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chatapp.R;
import com.chatapp.model.Message;

public class MessagesAdapter extends ArrayAdapter<Message> {
	private List<Message> mMessages;
	private LayoutInflater mInflater;
	private int mResource;
	private Context mContext;

	public MessagesAdapter(Context context, int resource, List<Message> messages) {
		super(context, resource, messages);
		this.mContext = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mMessages = messages;
		this.mResource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MessageHolder holder;
		final Message message = mMessages.get(position);
		if (convertView == null) {
			holder = new MessageHolder();
			convertView = mInflater.inflate(mResource, parent, false);
			holder.dAvatar = (ImageView) convertView
					.findViewById(R.id.msg_friend_avatar);
			holder.from = (TextView) convertView.findViewById(R.id.msg_from);
			holder.msgPartial = (TextView) convertView
					.findViewById(R.id.msg_partial_text);
			holder.time = (TextView) convertView.findViewById(R.id.msg_time);
			convertView.setTag(holder);
		} else {
			holder = (MessageHolder) convertView.getTag();
		}
		holder.dAvatar.setImageDrawable(mContext.getResources().getDrawable(
				R.drawable.profile));
		holder.from.setText(message.getFrom());
		holder.msgPartial.setText(message.getMessage().substring(0, 35));
		holder.time.setText("22:30");
		return convertView;
	}

	private static class MessageHolder {
		private ImageView dAvatar;
		private TextView from;
		private TextView msgPartial;
		private TextView time;
	}

}
