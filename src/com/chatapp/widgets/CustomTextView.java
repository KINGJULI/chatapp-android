package com.chatapp.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.chatapp.R;

public class CustomTextView extends TextView {

	private StringBuffer fontName;

	public CustomTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.custom_attr);
		fontName = new StringBuffer();
		fontName.append("fonts/");
		fontName.append(a.getString(R.styleable.custom_attr_font));
		System.err.println(fontName);
		setTypeface(context);
		setBackground(context);
		a.recycle();
	}

	public CustomTextView(Context context) {
		super(context);
		setTypeface(context);
	}

	private void setTypeface(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(),
				fontName.toString());
		this.setTypeface(tf);
	}

	private void setBackground(Context context) {
		this.setBackgroundResource(android.R.color.transparent);
	}
}
